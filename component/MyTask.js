import {StyleSheet} from 'react-native';
import React from 'react';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Today from './Today';
import Month from './Month';


const Tab = createMaterialTopTabNavigator();

const MyTask = () => {
  return (
    <Tab.Navigator style={{zIndex:1}} screenOptions={{
       tabBarActiveTintColor:'white',
       tabBarStyle: { backgroundColor: '#f96060' },
       tabBarIndicatorStyle:{backgroundColor:'white', width:'40%',height:3,marginHorizontal:20},
       tabBarLabelStyle:{
        fontSize:17,
        fontWeight:'600'
       }
    }}>
    <Tab.Screen name="Today" component={Today} options={{
      
       
    }} />
    <Tab.Screen name="Month" component={Month} 
    
    />
  </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
    
});

export default MyTask;


   


