import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import QuickNotes from './QuickNotes';
import MyTask from './MyTask';
import Menu from './Menu';
import Profile from './Profile';
import Add from './Add';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
const Tab = createBottomTabNavigator();
const CustomTabBarButton = ({children, onPress}) => (
  <TouchableOpacity style={styles.tabBarButton} onPress={onPress}>
    <View style={styles.redCircle}>
      <View style={styles.Plus}>{children}</View>
    </View>
  </TouchableOpacity>
);
const Bottomtab = () => {
  const navigation = useNavigation();
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {backgroundColor: '#292e4e', height: 80},
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'gray',
      }}
      tabBarOptions={{
        labelPosition: 'below-icon',
        labelStyle: {
          marginBottom: 10,
          fontSize: 15,
        },
      }}>
      <Tab.Screen
        name="My Task"
        component={MyTask}
        options={{
         title:'Work List',
         headerTitleStyle:{
          fontSize:23,
          color:'white',
         },
          tabBarLabel: 'MyTask',
          headerTitleAlign: 'center',
          headerStyle:{
            backgroundColor:'#f96060',
            height:100
          },
          headerRight:()=>(
            <View style={{marginRight:15}}>
            <TouchableOpacity onPress={()=>navigation.navigate('Menu')}>
              <Image
              style={{height:25,width:25,tintColor:'white'}}
              source={require('./assetes/filter.png')}
              />
            </TouchableOpacity>
            </View>
          ),
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <Image
                source={require('./assetes/tick.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ) : (
              <Image
                source={require('./assetes/tick.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Menu"
        component={Menu}
        options={{
          tabBarLabel: 'Menu',
          headerTitleAlign: 'center',
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <Image
                source={require('./assetes/menu.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ) : (
              <Image
                source={require('./assetes/menu.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Add"
        component={Add}
        options={{
          title: 'Add Note',
          headerTitleStyle: {
            fontSize: 23,
            color: 'white',
            marginTop: -40,
          },
          tabBarLabel: '',
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#f96060',
            height: 150,
          },

          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <Image
                source={require('./assetes/plus.png')}
                style={{width: 20, height: 20, tintColor: 'white'}}
              />
            ) : (
              <Image
                source={require('./assetes/plus.png')}
                style={{width: 20, height: 20, tintColor: 'white'}}
              />
            ),
          tabBarButton: props => <CustomTabBarButton {...props} />,
        }}
      />
      <Tab.Screen
        name="Quick Notes"
        component={QuickNotes}
        options={{
          tabBarLabel: 'Quick Notes',
          headerTitleAlign: 'center',

          headerTitleStyle: {
            fontSize: 25,
            opacity: 0.8,
          },
          headerTitleAlign: 'center',
          headerStyle: {
            height: 100,
          },
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <Image
                source={require('./assetes/notebook.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ) : (
              <Image
                source={require('./assetes/notebook.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          headerTitleAlign: 'center',
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <Image
                source={require('./assetes/user.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ) : (
              <Image
                source={require('./assetes/user.png')}
                style={{width: 35, height: 35, tintColor: color}}
              />
            ),
        }}
      />
    </Tab.Navigator>
  );
};
const styles = StyleSheet.create({
  tabBarButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redCircle: {
    width: 60,
    height: 60,
    borderRadius: 35,
    backgroundColor: '#e17070',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
    position: 'relative',
  },
  Plus: {
    bottom: -16,
  },
});

export default Bottomtab;
