
import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Pressable, KeyboardAvoidingView, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { setUser } from './redux/actions/authActions';

const SignUp = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const handleSignUp = async () => {

    setPasswordError('');
    setConfirmPasswordError('');

    
    let isValid = true;

    
    const passwordRegex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!password.trim() || !passwordRegex.test(password)) {
      setPasswordError(
        'Password should contain uppercase and lowercase letters, numbers, special characters, and have a minimum length of 8 characters',
      );
      isValid = false;
    }

    if (password !== confirmPassword) {
      setConfirmPasswordError('Passwords do not match');
      isValid = false;
    }

    if (isValid) {
    
      const existingUsersString = await AsyncStorage.getItem('users');
      const existingUsers = existingUsersString ? JSON.parse(existingUsersString) : [];

    
      const isUsernameExist = existingUsers.some(user => user.username === username);
      if (isUsernameExist) {
        alert('Username already exists. Please choose a different username.');
        return;
      }

  
      const newUser = { username, password };
      existingUsers.push(newUser);

  
      await AsyncStorage.setItem('users', JSON.stringify(existingUsers));


      dispatch(setUser({ username }));


      navigation.navigate('Bottomtab');
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.Container}>
      <View style={styles.Con2}>
        <Text style={styles.Text1}>Welcome </Text>
        <Text style={styles.Text2}>Sign up to continue</Text>
      </View>
      <View style={styles.Con3}>
        <View style={{ marginTop: '3%' }}>
          <Text style={styles.Text3}>Username</Text>
        </View>
        <View style={{ marginTop: '3%' }}>
          <TextInput
            style={styles.TextInput1}
            placeholder="Enter your mail-id here"
            value={username}
            onChangeText={text => setUsername(text)}
          />
        </View>
        <Text style={styles.Text4}>Password</Text>

        <TextInput
          style={styles.TextInput1}
          placeholder="Enter your Password"
          secureTextEntry
          value={confirmPassword}
          onChangeText={text => setConfirmPassword(text)}
        />
        <Text style={styles.errorText}>{passwordError}</Text>
        <Text style={styles.Text4}>Confirm Password</Text>
        <TextInput
          style={styles.TextInput1}
          placeholder="Confirm your Password"
          secureTextEntry
          value={password}
          onChangeText={text => setPassword(text)}
        />
        <Text style={styles.errorText}>{confirmPasswordError}</Text>
      </View>
      <View style={{ marginTop: '5%' }}>
        <Pressable style={styles.PressableText}>
          <Text style={styles.Text5}>Log In</Text>
        </Pressable>
      </View>

      <View style={styles.Con4}>
        <Pressable style={styles.PressableBtn} onPress={handleSignUp}>
          <Text style={styles.Btn}>Sign Up</Text>
        </Pressable>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    flexDirection: 'column',
     backgroundColor:'#ffffff'
    //marginHorizontal: 20,
  },
  Con1: {
    height: '10%',
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  Con2: {
    marginHorizontal: 20,
    marginTop: '12%',
  },
  Text1: {
    color: 'black',
    fontSize: 35,
  },
  Text2: {
    fontSize: 20,
    marginTop: 8,
  },
  Con3: {
    height: '40%',
    justifyContent: 'flex-start',
    width: '90%',
    marginHorizontal: 20,
    marginTop: '10%',
  },
  Text3: {
    fontSize: 25,
    color: 'black',
    justifyContent: 'flex-start',
  },
  TextInput1: {
    borderBottomWidth: 0.2,
    color: 'black',
    fontSize: 18,
  },
  Text4: {
    fontSize: 25,
    color: 'black',
    marginTop: 20,
  },
  PressableText: {
    flexDirection: 'row-reverse',
    height: '5%',
    justifyContent: 'flex-start',
    marginTop: -20,
  },
  Text5: {
    color: 'black',
    fontSize: 20,
    marginHorizontal: 20,
  },
  Con4: {
    marginTop: '7%',
    height: '30%',
    justifyContent: 'flex-start',
  },
  PressableBtn: {
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#f96060',
    height: 50,
    width: '90%',
    borderRadius: 5, 
    marginTop: 30,
  },
  Btn: {
    color: 'white',
    fontSize: 20,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});

export default SignUp;



// import {
//   View,
//   Text,
//   StyleSheet,
//   TextInput,
//   KeyboardAvoidingView,
//   Pressable,
// } from 'react-native';
// import React, {useState} from 'react';
// import {useNavigation} from '@react-navigation/native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import {UseDispatch, useDispatch} from 'react-redux';
// import {setUser} from './redux/actions/authActions';

// const SignUp = () => {
//   const navigation = useNavigation();
//   const dispatch = useDispatch();

//   const [username, setUsername] = useState('');
//   const [password, setPassword] = useState('');
//   const [confirmPassword, setConfirmPassword] = useState('');

//   const [usernameError, setUsernameError] = useState('');
//   const [passwordError, setPasswordError] = useState('');
//   const [confirmPasswordError, setConfirmPasswordError] = useState('');

//   const handleSignUp = async () => {
//     setUsernameError('');
//     setPasswordError('');
//     setConfirmPasswordError('');


//     let isValid = true;

  
//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//     if (!username.trim() || !emailRegex.test(username)) {
//       setUsernameError('Please enter a valid email');
//       isValid = false;
//     }

//     const passwordRegex =
//       /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
//     if (!password.trim() || !passwordRegex.test(password)) {
//       setPasswordError(
//         'Password should contain uppercase and lowercase letters, numbers, special characters, and have a minimum length of 8 characters',
//       );
//       isValid = false;
//     }

//     if (password !== confirmPassword) {
//       setConfirmPasswordError('Passwords do not match');
//       isValid = false;
//     }

//     if (isValid) {
//       await AsyncStorage.setItem('Name', username);
//       await AsyncStorage.setItem('PASSWORD', password);
//       await AsyncStorage.setItem('CONFIRM_PASSWORD', confirmPassword);

//       dispatch(setUser({username}));

//       navigation.navigate('Bottomtab');
//     }
//   };

//   return (
//     <KeyboardAvoidingView
//       behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
//       style={styles.Container}>
//       <View style={styles.Con2}>
//         <Text style={styles.Text1}>Welcome </Text>
//         <Text style={styles.Text2}>Sign up to continue</Text>
//       </View>
//       <View style={styles.Con3}>
//         <View style={{marginTop: '3%'}}>
//           <Text style={styles.Text3}>Username</Text>
//         </View>
//         <View style={{marginTop: '3%'}}>
//           <TextInput
//             style={styles.TextInput1}
//             placeholder="Enter your mail-id here"
//             value={username}
//             onChangeText={text => setUsername(text)}
//           />
//         </View>
//         <Text style={styles.Text4}>Password</Text>

//         <TextInput
//           style={styles.TextInput1}
//           placeholder="Enter your Password"
//           secureTextEntry
//           value={confirmPassword}
//           onChangeText={text => setConfirmPassword(text)}
//         />
//         <Text style={styles.errorText}>{passwordError}</Text>
//         <Text style={styles.Text4}>Confirm Password</Text>
//         <TextInput
//           style={styles.TextInput1}
//           placeholder="Confirm your Password"
//           secureTextEntry
//           value={password}
//           onChangeText={text => setPassword(text)}
//         />
//         <Text style={styles.errorText}>{confirmPasswordError}</Text>
//       </View>
//       <View style={{marginTop: '5%'}}>
//         <Pressable style={styles.PressableText}>
//           <Text style={styles.Text5}>Log In</Text>
//         </Pressable>
//       </View>

//       <View style={styles.Con4}>
//         <Pressable style={styles.PressableBtn} onPress={handleSignUp}>
//           <Text style={styles.Btn}>Sign Up</Text>
//         </Pressable>
//       </View>
//     </KeyboardAvoidingView>
//   );
// };

// const styles = StyleSheet.create({
//   Container: {
//     flex: 1,
//     flexDirection: 'column',
//     backgroundColor:'#ffffff'
//   },
//   Con1: {
//     height: '10%',
//     justifyContent: 'center',
//     marginHorizontal: 20,
//   },

//   Con2: {
//     marginHorizontal: 20,
//     marginTop: '12%',
//   },
//   Text1: {
//     color: 'black',
//     fontSize: 35,
//   },
//   Text2: {
//     fontSize: 20,
//     marginTop: 8,
//   },
//   Con3: {
//     height: '40%',
//     justifyContent: 'flex-start',
//     width: '90%',
//     marginHorizontal: 20,
//     marginTop: '10%',
//   },
//   Text3: {
//     fontSize: 25,
//     color: 'black',
//     justifyContent: 'flex-start',
//   },
//   TextInput1: {
//     borderBottomWidth: 0.2,
//     color: 'black',
//     fontSize: 18,
//   },
//   Text4: {
//     fontSize: 25,
//     color: 'black',
//     marginTop: 20,
//   },
//   PressableText: {
//     flexDirection: 'row-reverse',
//     height: '5%',
//     justifyContent: 'flex-start',
//     marginTop: -20,
//   },
//   Text5: {
//     color: 'black',
//     fontSize: 20,
//     marginHorizontal: 20,
//   },
//   Con4: {
//     marginTop: '7%',
//     height: '30%',
//     justifyContent: 'flex-start',
//   },
//   PressableBtn: {
//     justifyContent: 'center',
//     alignSelf: 'center',
//     backgroundColor: '#f96060',
//     height: 50,
//     width: '90%',
//     borderRadius: 8,
//     marginTop: 30,
//   },
//   Btn: {
//     color: 'white',
//     fontSize: 20,
//     alignSelf: 'center',
//     justifyContent: 'center',
//   },
// });
// export default SignUp;



