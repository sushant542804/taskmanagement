import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector} from 'react-redux';

const db = SQLite.openDatabase({name: 'sushant.db', location: 'default'});

const QuickNotes = () => {
  const [notes, setNotes] = useState([]);
  const [username, setUsername] = useState('');
  const reduxNotes = useSelector(state => state.notes.notes);

  useEffect(() => {
    const fetchUsername = async () => {
      try {
        const storedUsername = await AsyncStorage.getItem('Name');
        if (storedUsername) {
          console.log('Fetched username from AsyncStorage:', storedUsername);
          setUsername(storedUsername);
        }
      } catch (error) {
        console.error('Error retrieving username from AsyncStorage', error);
      }
    };

    fetchUsername();
  }, []);

  useEffect(() => {
    console.log('Fetching notes for username:', username);
    fetchNotesFromDatabase();
  }, [username, reduxNotes]);

  const fetchNotesFromDatabase = () => {
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, note TEXT, color TEXT)',
        [],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Table "notes" created or already exists');
          }
        },
        error => {
          console.error('Error creating table "notes"', error);
        },
      );

      tx.executeSql(
        'SELECT * FROM notes WHERE username = ?',
        [username],
        (tx, results) => {
          const len = results.rows.length;
          const notesArray = [];
          for (let i = 0; i < len; i++) {
            const row = results.rows.item(i);
            const existingNote = reduxNotes.find(
              reduxNote =>
                reduxNote.note === row.note && reduxNote.color === row.color,
            );
            if (!existingNote) {
              notesArray.push({note: row.note, color: row.color});
            }
          }
          setNotes(notesArray);
        },
        error => {
          console.error('Error retrieving notes from SQLite database', error);
        },
      );
    });
  };

  useEffect(() => {
    console.log('Username:', username);
    console.log('SQLite Notes:', notes);
    console.log('Redux Notes:', reduxNotes);
  }, [username, notes, reduxNotes]);

  return (
    <View style={styles.container}>
      <FlatList
        data={notes.concat(reduxNotes)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <View style={styles.subContainer}>
            <View
              style={[
                styles.Con2,
                {
                  borderTopColor: item.color,
                  borderTopWidth: 4,
                  width: '50%' || 'white',
                },
              ]}></View>
            <Text style={styles.note}>{item.note}</Text>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    zIndex: 1,
    backgroundColor: '#ffffff',
    marginHorizontal: '5%',
    margin: 10,
    marginTop: 20,
    borderRadius: 5,
    elevation: 2,
    width: '90%',
    paddingHorizontal: 20,
  },
  note: {
    fontSize: 20,
    padding: 6,
    fontWeight: '500',
    color: '#242020',
    opacity: 0.8,
    marginBottom: 11,
  },
  Con2: {
    height: 4,
    marginBottom: 8,
  },
});

export default QuickNotes;
