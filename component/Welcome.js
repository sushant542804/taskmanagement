import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Pressable,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

const Img = require('./assetes/welcome.png');
const logoImg2 = require('./assetes/welcome2.png');

const Welcome = () => {
  const navigation = useNavigation();
  const getStarted = () => {
    navigation.navigate('SignUp');
  };
  const getLogin = () => {
    navigation.navigate('Login');
  };

  return (
    <View style={styles.Con1}>
      <View style={styles.Image1}>
        <Image source={logoImg2} style={{height: 250, width: 250}} />
      </View>
      <View style={styles.textInput1}>
        <Text style={styles.welcomeText}>Welcome To aking</Text>
        <Text style={styles.additionalText}>
          {' '}
          Whats going to happen tommarow ?
        </Text>
      </View>
      <View style={styles.Image2}>
        <ImageBackground source={Img} style={{height: '100%', width: '100%'}}>
          <Pressable style={styles.btn1} onPress={getStarted}>
            <Text style={styles.btntext1}>Get Started</Text>
          </Pressable>
          <Pressable style={styles.btn2} onPress={getLogin}>
            <Text style={styles.LoginText}>Log In</Text>
          </Pressable>
        </ImageBackground>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Con1: {
    flex: 1,
    flexDirection: 'column',
  },
  Image1: {
    height: '50%',
    alignItems: 'center',
    marginTop: '20%',
  },
  Image2: {
    justifyContent: 'flex-end',
    height: '60%',
  },
  btn1: {
    width: '80%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 17,
    marginTop: '27%',
    marginHorizontal: 45,
    borderRadius: 10,
  },
  btn2: {
    alignItems: 'center',
    marginHorizontal: 45,
    borderRadius: 10,
    marginTop: 20,
  },
  btntext1: {
    fontSize: 18,
    fontWeight: '500',
    color: 'black',
    opacity: 0.7,
  },
  textInput1: {
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: -90,
    marginBottom: 25,
  },
  welcomeText: {
    fontSize: 26,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  additionalText: {
    fontSize: 20,
  },
  LoginText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default Welcome;
