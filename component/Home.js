
import React, { useEffect } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const imgscreen1 = require('./assetes/screen1.png');

const Home = () => {
  const navigation = useNavigation();

  useEffect(() => {
    const timer = setTimeout(() => {
      Navigation()
    }, 3000);

    const Navigation= async () =>{
      
      const data= await AsyncStorage.getItem("isLogin")
      if(data === 'true'){
        navigation.navigate('Bottomtab');
      } else{
        navigation.replace('Welcome');
      }
    }

    return () => clearTimeout(timer);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image source={imgscreen1} style={styles.imageinput} />
      <Text style={styles.textinput}>aking</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '70%',
  },
  imageinput: {
    height: 200,
    width: 200,
  },
  textinput: {
    fontSize: 35,
    padding: 10,
    color: 'black',
    fontWeight: '500',
    opacity: 0.8,
  },
});

export default Home;
