import { Pressable, StyleSheet, Text, View, Alert } from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Profile = () => {
  const navigation = useNavigation();

  const LogOut = async () => {
    Alert.alert(
      'Log Out',
      'Are you sure you want to log out?',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: async () => {
            await AsyncStorage.setItem('isLogin', 'false');
            navigation.navigate('Welcome');
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <View>
      <View>
        <Pressable style={styles.logout} onPress={LogOut}>
          <Text style={styles.textlogout}> LogOut </Text>
        </Pressable>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  logout: {
    backgroundColor: '#F96060',
    padding: 15,
    alignItems: 'center',
    margin: 25,
    borderRadius: 5,
  },
  textlogout: {
    fontSize: 20,
    color: 'white',
  },
});