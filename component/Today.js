import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Swipelist from 'react-native-swipeable-list-view';
import SQLite from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector} from 'react-redux';

const db = SQLite.openDatabase({name: 'sushant.db', location: 'default'});

const edit = require('./assetes/edit2.png');
const imgdelet = require('./assetes/delete2.png');

const Today = () => {
  const [notes, setNotes] = useState([]);
  const [username, setUsername] = useState('');
  const reduxNotes = useSelector(state => state.notes.notes);

  useEffect(() => {
    const fetchUsername = async () => {
      try {
        const storedUsername = await AsyncStorage.getItem('Name');
        if (storedUsername) {
          console.log('Fetched username from AsyncStorage:', storedUsername);
          setUsername(storedUsername);
        }
      } catch (error) {
        console.error('Error retrieving username from AsyncStorage', error);
      }
    };

    fetchUsername();
  }, []);

  useEffect(() => {
    console.log('Fetching notes for username:', username);
    fetchNotesFromDatabase();
  }, [username, reduxNotes]);

  const fetchNotesFromDatabase = () => {
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, note TEXT, color TEXT)',
        [],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Table "notes" created or already exists');
          }
        },
        error => {
          console.error('Error creating table "notes"', error);
        },
      );

      tx.executeSql(
        'SELECT * FROM notes WHERE username = ?',
        [username],
        (tx, results) => {
          const len = results.rows.length;
          const notesArray = [];
          for (let i = 0; i < len; i++) {
            const row = results.rows.item(i);
            const existingNote = reduxNotes.find(
              reduxNote =>
                reduxNote.note === row.note && reduxNote.color === row.color,
            );
            if (!existingNote) {
              notesArray.push({note: row.note, color: row.color});
            }
          }
          setNotes(notesArray);
        },
        error => {
          console.error('Error retrieving notes from SQLite database', error);
        },
      );
    });
  };

  useEffect(() => {
    console.log('Username:', username);
    console.log('SQLite Notes:', notes);
    console.log('Redux Notes:', reduxNotes);
  }, [username, notes, reduxNotes]);

  const deleteNote = item => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM notes WHERE username = ? AND note = ?',
        [username, item.note],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            console.log('Note deleted successfully');
            fetchNotesFromDatabase();
          } else {
            console.log('No note found to delete');
          }
        },
        error => {
          console.error('Error deleting note from SQLite database', error);
        },
      );
    });
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <Swipelist
        data={notes.concat(reduxNotes)}
        renderRightItem={(item, index) => (
          <View key={item} style={styles.container}>
            <View
              style={[
                styles.Con2,
                {
                  borderRightColor: item.color,
                  borderRightWidth: 5,
                },
              ]}></View>
            <Text style={styles.note}>{item.note}</Text>
          </View>
        )}
        renderHiddenItem={(item, index) => (
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={[styles.rightAction, {backgroundColor: '#ffffff'}]}
              onPress={() => {
                Alert.alert('Edit?', item.note);
              }}>
              <Image source={edit} style={{width: 25, height: 25}} />
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.rightAction, {backgroundColor: '#ffffff'}]}
              onPress={() => {
                Alert.alert(
                  'Delete?',
                  item.note,
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {
                      text: 'OK',
                      onPress: () => deleteNote(item),
                    },
                  ],
                  {cancelable: false},
                );
              }}
              rightOpenValue={200}>
              <Image source={imgdelet} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
        )}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    marginHorizontal: '5%',
    marginVertical: 15,
  },
  container: {
    //  height: 60,
    borderRadius: 5,
    marginVertical: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 5,
    marginRight: 5,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  rightAction: {
    width: '100%',
    marginVertical: 10,
    borderRadius: 3,
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    // height: 60,
    padding: 15,
    backgroundColor: '#ffffff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  note: {
    fontSize: 20,
    padding: 8,
    fontWeight: '500',
    color: '#242020',
    opacity: 1,
    marginBottom: 11,
  },
  Con2: {
    position: 'absolute',
    top: 13,
    bottom: 13,
    right: 0,
  },
});

export default Today;
