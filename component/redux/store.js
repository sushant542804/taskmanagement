
import { createStore, combineReducers } from 'redux';
import authReducer from './reducers/authReducers';
import noteReducer from './reducers/noteReducers'; 

const rootReducer = combineReducers({
  auth: authReducer,
  notes: noteReducer, 
});

const store = createStore(rootReducer);

export default store;
