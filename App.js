
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Welcome from './component/Welcome';
import Home from './component/Home';
import Login from './component/Login';
import SignUp from './component/SignUp';
import MyTask from './component/MyTask'; 
import Bottomtab from './component/Bottomtab';
import Add from './component/Add';
import QuickNotes from './component/QuickNotes';
import Profile from './component/Profile';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Welcome" component={Welcome} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="MyTask" component={MyTask} /> 
        <Stack.Screen name="Bottomtab" component={Bottomtab} options={{ headerShown: false }}/> 
        <Stack.Screen name="Add" component={Add} />
        <Stack.Screen name="QuickNotes" component={QuickNotes}
         options={{
          headerTitle:'Quick Notes',
          headerTitleStyle:{
             fontSize:25,
             opacity:0.8
          },
          headerTitleAlign:'center',
          headerStyle:{
            height:100
          }
         }}
        />
        <Stack.Screen name="Profile" component={Profile}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

//C:\Users\Public\Documents\TaskManangement\TaskModule1.jks
// password:developer